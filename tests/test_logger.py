# -*- coding: utf-8 -*-
"""
    Test cases related to the Logger class

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
import unittest

try:
    # Python 3.x
    from unittest.mock import Mock, patch
except ImportError:
    from mock import Mock, patch

import xmlrunner # pylint: disable=unused-import
# This import is required for generating the XML test results


from tests.xbmc_mocks import xbmc_mock
from resources.lib.logger import Logger


class TestLogger(unittest.TestCase):
    """Test cases related to the class Logger"""

    def setUp(self):
        """Set up actions

        Set "longMessage" to True to print the classic error message and the
        user "msg" from the assert() function in case of failures.
        """
        # In addition to the classic error message when a test case fails,
        # also display the "msg" string from the assert() function
        self.longMessage = True

        # Hard-code the add-on name with the correct type depending on the
        # python version (normally it is retrieved from xbmcaddon and it could
        # have been retrieved from addon.xml for the tests)
        self.addon_name = u"ImagoTV"
        if sys.version_info.major == 2:
            self.addon_name = self.addon_name.encode("utf-8")

        # Create a Logger instance for all the test cases using a mocked
        # instance of GlobalContext
        self.mocked_context = Mock()
        self.mocked_context.addon.getAddonInfo.return_value = self.addon_name
        self.logger = Logger(self.mocked_context)

        # Create string of specific types that will be reused in the test cases
        self.unicode_string = u"Mÿ strîng containing non-ASCII chàractérs."
        self.byte_string = self.unicode_string.encode("utf-8")

    @patch("resources.lib.logger.Logger._prefix")
    def test_logger_fatal(self, prefix_mock):
        """Test Logger.fatal()

        Note: Logger._prefix() and the xbmc module are mocked.
        """

        # Call the method and check the Exception was raised
        with self.assertRaises(Exception):
            self.logger.fatal(self.unicode_string, Exception, True)
        # Check xbmc.log was called with the expected parameters
        xbmc_mock.log.assert_called_once_with(
            prefix_mock(self.unicode_string),
            xbmc_mock.LOGFATAL
        )

        # Now call again the method but use the message in the arguments as
        # Exception message
        with self.assertRaises(Exception) as e:
            self.logger.fatal(self.unicode_string, Exception, False)
        # Check (outside of the context manager) that the Exception was raised
        # with the expected message i.e. the returned value of _prefix().
        # Note: e.exception must converted with str() because
        # BaseException.message is deprecated in python3
        self.assertEqual(str(prefix_mock.return_value), str(e.exception))

        # Reset the calls of the mocked xbmc module because some other test
        # cases will assert xbmc.log was called only once
        xbmc_mock.reset_mock()

    @patch("resources.lib.logger.Logger._prefix")
    def test_logger_error(self, prefix_mock):
        """Test Logger.error()

        Note: Logger._prefix() and the xbmc module are mocked.
        """

        # Call the method and check the call to xbmc.log was done with the
        # expected parameters
        self.logger.error(self.unicode_string)
        xbmc_mock.log.assert_called_once_with(
            prefix_mock(self.unicode_string),
            xbmc_mock.LOGERROR
        )

        # Reset the calls of the mocked xbmc module because some other test
        # cases will assert xbmc.log was called only once
        xbmc_mock.reset_mock()

    @patch("resources.lib.logger.Logger._prefix")
    def test_logger_debug(self, prefix_mock):
        """Test Logger.debug()

        Note: Logger._prefix() and the xbmc module are mocked.
        """

        # Call the method and check the call to xbmc.log was done with the
        # expected parameters
        self.logger.debug(self.unicode_string)
        xbmc_mock.log.assert_called_once_with(
            prefix_mock(self.unicode_string),
            xbmc_mock.LOGDEBUG
        )

        # Reset the calls of the mocked xbmc module because some other test
        # cases will assert xbmc.log was called only once
        xbmc_mock.reset_mock()

    @patch("resources.lib.logger.Logger._prefix")
    def test_logger_warning(self, prefix_mock):
        """Test Logger.warning()

        Note: Logger._prefix() and the xbmc module are mocked.
        """

        # Call the method and check the call to xbmc.log was done with the
        # expected parameters
        self.logger.warning(self.unicode_string)
        xbmc_mock.log.assert_called_once_with(
            prefix_mock(self.unicode_string),
            xbmc_mock.LOGWARNING
        )

        # Reset the calls of the mocked xbmc module because some other test
        # cases will assert xbmc.log was called only once
        xbmc_mock.reset_mock()

    def test_logger_convert(self):
        """Test Logger._convert()"""

        # Store the return value of the function a the unicode string is used
        converted_string = self.logger._convert(self.unicode_string)

        if sys.version_info.major == 2:
            # In python2, check that the returned value is the same as the
            # original unicode string but converted to a byte string
            self.assertEqual(converted_string, self.byte_string)
            self.assertIsInstance(converted_string, bytes)
        else:
            # In python3, self.context.unicode() must be called
            self.mocked_context.unicode.assert_called_once_with(
                self.unicode_string
            )

        # Now call the function with the string converted to a byte string
        new_converted_string = self.logger._convert(self.byte_string)

        if sys.version_info.major == 2:
            # Check the returned value is:
            #  - the same as the original byte string
            #  - a byte string
            self.assertEqual(new_converted_string, self.byte_string)
            self.assertIsInstance(new_converted_string, bytes)
        # Nothing more to test for python3 (the code is the same whether a byte
        # string is used or an unicode string is used.)

    def test_logger_prefix(self):
        """Test Logger._prefix()"""

        # Call the function with an unicode string
        prefixed_string = self.logger._prefix(self.unicode_string)

        # Check the returned value and the type depending on the python version
        if sys.version_info.major == 2:
            self.assertEqual(
                prefixed_string, "[{}] {}".format(self.addon_name,
                                                  self.byte_string)
            )
            self.assertIsInstance(prefixed_string, bytes)
        else:
            # In python3 "_convert()" (which is called by "_prefix()") will
            # call "context.unicode()" which is mocked so the full value
            # cannot be checked. Instead check type and check that the
            # addon_name is in the returned string.
            self.assertIsInstance(prefixed_string, str)
            self.assertIn("[{}] ".format(self.addon_name), prefixed_string)
