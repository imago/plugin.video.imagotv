# -*- coding: utf-8 -*-
"""
    Test cases related to the adon entry point

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import unittest

try:
    # Python 3.x
    from unittest.mock import patch
except ImportError:
    from mock import patch

import tests.xbmc_mocks  # pylint: disable=unused-import
# This import is required to mock the xbmcaddon module imported in globals
import main
import xmlrunner # pylint: disable=unused-import
# This import is required for generating the XML test results

class TestMain(unittest.TestCase):
    """Test cases related to the main.py file"""

    def setUp(self):
        """Set up actions

        Set "longMessage" to True to print the classic error message and the
        user "msg" from the assert() function in case of failures.
        """
        # In addition to the classic error message when a test case fails,
        # also display the "msg" string from the assert() function
        self.longMessage = True

        # Create a dummy value to replace sys.argv
        self.sys_argv = ["plugin://", 0, "?"]

    @patch("main.GlobalContext")
    @patch("main.router")
    def test_main(self, router, global_context):
        """Test the main function when __name__ is "__main__" """

        # Set the value of __name__ with "__main__"
        with patch.object(main, "__name__", "__main__"):
            # Call the main function
            main.main_func(self.sys_argv)

            # Check that all the expected calls were done.
            global_context.assert_called_once_with(argv=self.sys_argv)
            context = global_context.return_value
            router.assert_called_once_with(context)

    @patch("main.GlobalContext")
    @patch("main.router")
    def test_main_wrong__name__(self, router, global_context):
        """Test nothing is done when __name__ is not "__main__" """

        # Set the value of __name__ with something else than "__main__"
        with patch.object(main, "__name__", "__not_main__"):
            # Call the main function
            main.main_func(self.sys_argv)

            # Check that nothing else was called
            global_context.assert_not_called()
            context = global_context.return_value
            context.kodi.display_popup.assert_not_called()
            router.assert_not_called()
