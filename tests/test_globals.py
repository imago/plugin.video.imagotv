# -*- coding: utf-8 -*-
"""
    Test cases related to the GlobalContext class

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import unittest

import xmlrunner # pylint: disable=unused-import
# This import is required for generating the XML test results

import tests.xbmc_mocks # pylint: disable=unused-import
# This import is required to mock the xbmcaddon module imported in globals
from resources.lib.globals import GlobalContext

class TestGlobalContext(unittest.TestCase):
    """Test cases related to the GlobalContext class"""

    def setUp(self):
        """Set up actions

        Set "longMessage" to True to print the classic error message and the
        user "msg" from the assert() function in case of failures.
        """
        # In addition to the classic error message when a test case fails,
        # also display the "msg" string from the assert() function
        self.longMessage = True

        self.context = GlobalContext(argv=["plugin://", 0, "?"])

    def test_globalcontext_unicode(self):
        """Test "GlobalContext.unicode()" returns an unicode string"""

        # Create an unicode string containing non-ASCII characters.
        unicode_string = u"Mÿ strîng containing non-ASCII chàractérs."

        # Store the return value of the function when this string is used
        converted_string = self.context.unicode(unicode_string)

        # Check the returned value is:
        #  - the same as the original string
        #  - an unicode string (In python2 "unicode" is the type of unicode
        # strings whereas it is "str" in python3. Therefore to have generic
        # code assertIsInstance() is replaced by assertTrue() with type().)
        self.assertEqual(converted_string, unicode_string)
        self.assertTrue(type(converted_string) == type(unicode_string))

        # Now call the function with the string converted to a byte string
        byte_string = unicode_string.encode("utf-8")
        new_converted_string = self.context.unicode(byte_string)

        # Check the returned value is:
        #  - the same as the original unicode string
        #  - an unicode string
        self.assertEqual(new_converted_string, unicode_string)
        self.assertTrue(type(new_converted_string) == type(unicode_string))
