# -*- coding: utf-8 -*-
"""
    Test cases related to the KodiUtils class

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import unittest

try:
    # Python 3.x
    from unittest.mock import ANY, call, Mock, patch
except ImportError:
    from mock import ANY, call, Mock, patch

import xmlrunner # pylint: disable=unused-import
# This import is required for generating the XML test results

from tests.xbmc_mocks import xbmc_mock, xbmcplugin_mock, xbmcgui_mock
from resources.lib.kodiutils import KodiUtils


class TestKodiUtils(unittest.TestCase):
    """Test cases related to the class KodiUtils"""

    def setUp(self):
        """Set up actions

        Set "longMessage" to True to print the classic error message and the
        user "msg" from the assert() function in case of failures.
        """
        # In addition to the classic error message when a test case fails,
        # also display the "msg" string from the assert() function
        self.longMessage = True

        # Create a KodiUtils object using a mocked GlobalContext object
        self.mocked_context = Mock()
        self.mocked_context.plugin_url = u"plugin://url"
        self.mocked_context.plugin_handle = 12
        self.kodi = KodiUtils(self.mocked_context)

    @patch("resources.lib.kodiutils.urlencode")
    def test_build_kodi_url(self, urlencode_mock):
        """Test build_kodi_url with several cases"""

        # Set the return value of the mocked urlencode
        urlencode_mock.return_value = u"name=value"

        # Call the method without "plugin_url" and check the return value
        url = self.kodi.build_kodi_url(parameters={})
        self.assertEqual(url, u"{}?name=value"
                         .format(self.mocked_context.plugin_url))
        urlencode_mock.assert_called_once_with({})

        urlencode_mock.reset_mock()

        # Call the method with "plugin_url" and check the return value
        url = self.kodi.build_kodi_url(parameters={}, plugin_url="url://url")
        self.assertEqual(url, u"url://url?name=value")
        urlencode_mock.assert_called_once_with({})

    @patch("resources.lib.kodiutils.xbmcgui.ListItem")
    @patch("resources.lib.kodiutils.KodiUtils._set_title")
    @patch("resources.lib.kodiutils.KodiUtils._add_sort_methods")
    @patch("resources.lib.kodiutils.KodiUtils._set_viewtype")
    def test_generate_list_of_items(self, set_view_type, add_sort_methods,
                                    set_title, listitem_mock):
        """Test generate_list_of_items"""

        # Create a list of items
        list_of_items = [
            {
                "name": "folder",
                "url": "folder_url",
                "is_folder": True,
                "action_type": "action_type",
                "thumbnail": "folder_thumbnail",
                "background": "folder_background",
                "description": "folder_description",
                "author": "folder_author",
                "duration": "folder_duration",
                "date": "folder_date"
            },
            {
                "name": "media_name",
                "url": "media_url",
                "is_folder": False,
                "action_type": "action_type",
                "thumbnail": "media_thumbnail",
                "background": "",
                "description": "media_description",
                "author": "media_author",
                "duration": "media_duration",
                "date": "media_date"
            }
        ]

        # Call the API with the list of items
        self.kodi.generate_list_of_items(list_of_items)

        # Check that the internal KodiUtils methods were called
        set_title.assert_called_once()
        add_sort_methods.assert_called_once()
        set_view_type.assert_called_once()

        # Check that all the calls to the Kodi API were made for each item in
        # the list
        for item in list_of_items:

            xbmcgui_mock.ListItem.assert_any_call(
                label=item["name"])

            listitem_mock.return_value.setArt.assert_any_call({
                "thumb": item["thumbnail"],
                "fanart":
                    item["background"] if item["background"] != ""
                    else self.mocked_context.addon.getAddonInfo('fanart')
            })

            listitem_mock.return_value.setInfo.assert_any_call(
                "video",
                {"plot": item["description"],
                 "title": item["name"],
                 "duration": item["duration"],
                 "aired": item["date"],
                 "credits": item["author"]})

        xbmcplugin_mock.addDirectoryItems.assert_called_once()

        xbmcplugin_mock.endOfDirectory.assert_called_once_with(
            self.mocked_context.plugin_handle)

    # Reset the ListItem mock because it is used in other test cases
    xbmcgui_mock.ListItem.reset_mock()

    @patch("resources.lib.kodiutils.KodiUtils.get_string")
    def test_set_title_home_page(self, get_string):
        """Test _set_title for the home page"""

        # Set the type of the current page to the home page
        self.mocked_context.type = self.mocked_context.TYPE_HOME_PAGE

        # Call the method
        self.kodi._set_title()

        # Check that setPluginCategory was correctly called
        xbmcplugin_mock.setPluginCategory.assert_called_once_with(
            handle=self.mocked_context.plugin_handle,
            category=get_string(30205)
        )

        # Reset the mock because it is used by other test cases.
        xbmcplugin_mock.setPluginCategory.reset_mock()

    def test_set_title_not_home_page(self):
        """Test _set_title for another page than the home page"""

        # Set the type of the current page to something else than the home page
        self.mocked_context.TYPE_HOME_PAGE = 1
        self.mocked_context.type = 2

        # Call the method
        self.kodi._set_title()

        # Check that setPluginCategory was correctly called
        xbmc_mock.getInfoLabel.assert_called_once_with("ListItem.Label")
        xbmcplugin_mock.setPluginCategory.assert_called_once_with(
            handle=self.mocked_context.plugin_handle,
            category=xbmc_mock.getInfoLabel.return_value
        )

        # Reset the mocks in case they are used by other test cases.
        xbmc_mock.getInfoLabel.reset_mock()
        xbmcplugin_mock.setPluginCategory.reset_mock()

    def test_add_sort_methods(self):
        """Test the expected sort methods are added depending on the type"""

        # 1st use case: type is TYPE_HOME_PAGE
        self.mocked_context.type = self.mocked_context.TYPE_HOME_PAGE
        self.kodi._add_sort_methods()
        xbmcplugin_mock.addSortMethod.assert_called_once_with(
            self.mocked_context.plugin_handle,
            xbmcplugin_mock.SORT_METHOD_NONE
        )
        xbmcplugin_mock.addSortMethod.reset_mock()

        # 2nd use case: type is TYPE_THEMES
        self.mocked_context.type = self.mocked_context.TYPE_THEMES
        self.kodi._add_sort_methods()
        xbmcplugin_mock.addSortMethod.assert_called_once_with(
            self.mocked_context.plugin_handle,
            xbmcplugin_mock.SORT_METHOD_LABEL
        )
        xbmcplugin_mock.addSortMethod.reset_mock()

        # 3rd use case: type is TYPE_SHOWS
        self.mocked_context.type = self.mocked_context.TYPE_SHOWS
        self.kodi._add_sort_methods()
        xbmcplugin_mock.addSortMethod.assert_called_once_with(
            self.mocked_context.plugin_handle,
            xbmcplugin_mock.SORT_METHOD_LABEL
        )
        xbmcplugin_mock.addSortMethod.reset_mock()

        # 4th use case: type is TYPE_EPISODES
        self.mocked_context.type = self.mocked_context.TYPE_EPISODES
        self.kodi._add_sort_methods()
        xbmcplugin_mock.addSortMethod.assert_has_calls(
            [
                call(self.mocked_context.plugin_handle,
                     xbmcplugin_mock.SORT_METHOD_TITLE),
                call(self.mocked_context.plugin_handle,
                     xbmcplugin_mock.SORT_METHOD_DATE),
                call(self.mocked_context.plugin_handle,
                     xbmcplugin_mock.SORT_METHOD_DURATION)
            ]
        )
        xbmcplugin_mock.addSortMethod.reset_mock()

    def test_set_viewtype_no_custom_viewtypes(self):
        """Test _set_viewtype when the customized viewtypes are disabled"""

        # Reset the mock because it was used by other test cases
        xbmc_mock.executebuiltin.reset_mock()

        # Mock the "enable customized viewtypes" setting to false
        self.mocked_context.addon.getSettingBool.return_value = False

        # Call the method
        self.kodi._set_viewtype()

    def test_set_viewtype_with_custom_viewtypes(self):
        """Test _set_viewtype when the customized viewtypes are enabled

        This test case validate the whole "switch case" (except the final else)
        so a structure is used to limit the amount of duplicate code.
        """

        # Mock the "enable customized viewtypes" setting to true
        self.mocked_context.addon.getSettingBool.return_value = True

        # Create a list with all the values required to validate the method
        # in all the cases
        test_data = [
            {"custom_viewtype_id": 500, "type": "TYPE_HOME_PAGE"},
            {"custom_viewtype_id": 51, "type": "TYPE_THEMES"},
            {"custom_viewtype_id": 55, "type": "TYPE_SHOWS"},
            {"custom_viewtype_id": 503, "type": "TYPE_EPISODES"}
        ]

        for data in test_data:

            # Reset the mock for this iteration
            xbmc_mock.executebuiltin.reset_mock()

            # Configure the return value for the customized viewtypes ID
            self.mocked_context.addon.getSetting.return_value = \
                data["custom_viewtype_id"]

            # Configure the type of the current window
            setattr(self.mocked_context, data["type"], 42)
            self.mocked_context.type = 42

            # Call the method
            self.kodi._set_viewtype()

            # Check that the function which sets the viewtype was called
            # with the expected ID
            xbmc_mock.executebuiltin.assert_called_once_with(
                "Container.SetViewMode({})".format(data["custom_viewtype_id"]))

            # Use a different value for the current enum TYPE_xxx so that the
            # condition in this "if" will not be true in the next iteration
            setattr(self.mocked_context, data["type"], 99)

    def test_set_viewtype_with_custom_viewtypes_unknown_type(self):
        """Test _set_viewtype when the customized viewtypes are enabled

        This test case validate the else clause i.e. when the type of the
        current window is unknown.
        """

        # Reset the mock because it was used by other test cases
        xbmc_mock.executebuiltin.reset_mock()

        # Mock the "enable customized viewtypes" setting to true
        self.mocked_context.addon.getSettingBool.return_value = True

        # Ensure the else clause will be reached because of an unknonw type of
        # the current window
        self.mocked_context.type = 999

        # Call the method
        self.kodi._set_viewtype()

        # Check that the function which sets the viewtype was called with the
        # expected ID
        xbmc_mock.executebuiltin.assert_called_once_with(
            "Container.SetViewMode({})".format(55))

    def test_play_media(self):
        """Test play_media()"""

        test_url = "my-url"
        # Call the  method with a test URL
        self.kodi.play_media(url=test_url)

        xbmcplugin_mock.setResolvedUrl.assert_called_once_with(
            self.mocked_context.plugin_handle,
            True,
            ANY
        )
        # As "ANY" is used for the last argument, the call to
        # xbmcgui.ListItem() is checked separately
        xbmcgui_mock.ListItem.assert_called_once_with(path=test_url)
        # Reset the ListItem mock because it is used in other test cases
        xbmcgui_mock.ListItem.reset_mock()

    def test_display_popup(self):
        """Test display_popup()"""

        title = "My Title"
        message = "My message!"

        # Case 1: call with only_debug=False (implicit)
        self.kodi.display_popup(title, message)
        xbmcgui_mock.Dialog.return_value.ok.assert_called_once_with(
            heading=title,
            line1=message)

        # Case 2: call with only_debug=True and debug_mode enabled
        xbmcgui_mock.reset_mock()
        self.kodi.context.debug_enabled = True
        self.kodi.display_popup(title, message, only_debug=True)
        xbmcgui_mock.Dialog.return_value.ok.assert_called_once_with(
            heading=title,
            line1=message)

        # Case 3: call with only_debug=True and debug_mode disabled
        xbmcgui_mock.reset_mock()
        self.kodi.context.debug_enabled = False
        self.kodi.display_popup(title, message, only_debug=True)
        xbmcgui_mock.Dialog.return_value.ok.assert_not_called()

    def test_open_input_dialog(self):
        """Test open_input_dialog()"""

        title = "My Title"
        default_value = "This is a default value."

        self.kodi.open_input_dialog(title, default_value)

        self.kodi.context.unicode.assert_called_once()
        xbmcgui_mock.Dialog.return_value.input.assert_called_once_with(
            heading=title,
            defaultt=default_value,
            type=xbmcgui_mock.INPUT_ALPHANUM
        )

    def test_create_dialog_progress(self):
        """Test create_dialog_progress()"""

        title = "My Title"
        message = "This is a message!"

        # Call the method when a DialogProgress box was not created
        self.kodi.progress_box = None
        self.kodi.create_dialog_progress(title, message)
        xbmcgui_mock.DialogProgress.return_value.create\
            .assert_called_once_with(heading=title,
                                     line1=message)
        xbmcgui_mock.DialogProgress.reset_mock()

        # Call the method when a DialogProgress box was already created
        self.kodi.create_dialog_progress(title, message)
        self.mocked_context.logger.error.assert_called_once_with(
            u"Cannot create a new DialogProgress with title {} because there"
            u" is already one created.".format(title)
        )
        xbmcgui_mock.DialogProgress.return_value.create.assert_not_called()


    def test_update_dialog_progress(self):
        """Test update_dialog_progress()"""

        # Value that will used when calling the method
        percent = 50

        # Call the method when a DialogProgress box was not created
        self.kodi.progress_box = None
        self.kodi.update_dialog_progress(percent)
        xbmcgui_mock.DialogProgress.return_value.update.assert_not_called()

        # Simulate the creation of a box and then call the method
        self.kodi.progress_box = Mock()
        self.kodi.update_dialog_progress(percent)
        self.kodi.progress_box.update.assert_called_once_with(percent=percent)

        # Reset the attribute associated with the DialogProgress box
        self.kodi.progress_box = None

    def test_close_dialog_progress(self):
        """Test close_dialog_progress()"""

        # Call the method when a DialogProgress box was not created
        self.kodi.progress_box = None
        self.kodi.close_dialog_progress()
        xbmcgui_mock.DialogProgress.return_value.close.assert_not_called()

        # Simulate the creation of a box and then call the method
        self.kodi.progress_box = Mock()
        self.kodi.close_dialog_progress()
        self.assertIsNone(self.kodi.progress_box)

    def test_is_dialog_cancelled(self):
        """Test is_dialog_cancelled()"""

        # Call the method when a DialogProgress box was not created
        self.kodi.progress_box = None
        self.assertEqual(self.kodi.is_dialog_cancelled(), False)
        xbmcgui_mock.DialogProgress.return_value.iscanceled.assert_not_called()

        # Simulate the creation of a box and then call the method
        self.kodi.progress_box = Mock()
        self.assertEqual(self.kodi.is_dialog_cancelled(),
                         self.kodi.progress_box.iscanceled.return_value)
        self.kodi.progress_box.iscanceled.assert_called_once()

    def test_get_string(self):
        """Test get_string()"""

        string_id = 30200

        self.kodi.get_string(string_id)
        self.mocked_context.addon.getLocalizedString\
            .assert_called_once_with(string_id)
