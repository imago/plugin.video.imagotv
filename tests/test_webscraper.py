# -*- coding: utf-8 -*-
"""
    Test cases related to web scraping

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import unittest

try:
    # Python 3.x
    from unittest.mock import Mock, patch, call
except ImportError:
    from mock import Mock, patch, call

# from requests import HTTPError
import xmlrunner # pylint: disable=unused-import
# This import is required for generating the XML test results

from resources.lib.webscraper import WebScraper, WebScraperError


class TestWebScraper(unittest.TestCase):
    """Test cases related to the WebScraper class"""

    def setUp(self):
        """Set up actions

        Set "longMessage" to True to print the classic error message and the
        user "msg" from the assert() function in case of failures.

        Create some WebScraper objects that can be reused in all the tests
        instead of creating a new object every time.
        """
        # In addition to the classic error message when a test case fails,
        # also display the "msg" string from the assert() function
        self.longMessage = True

        # Mock a GlobalContext object that will be used to create all the
        # WebScraper objects in this file.
        self.context = Mock()
        # Configure the Mock so that the code behaves correctly (otherwise the
        # break in _build_items_info will be reached every time for TYPE_SHOWS)
        self.context.kodi.is_dialog_cancelled.return_value = False

        # Create a dummy WebScraper object to use in several test cases
        self.dummy_page = WebScraper(context=self.context, url="")

    def test_define_page_type(self):
        """Test the different use cases in _define_page_type"""

        # Use case #1: empty URL
        self.dummy_page._define_page_type(u"")
        self.assertEqual(self.context.type, self.context.TYPE_HOME_PAGE)

        # Use case #2: using the search URL
        self.dummy_page._define_page_type(u"/recherche/words")
        self.assertEqual(self.context.type, self.context.TYPE_SHOWS)

        # Use case #3: URL of type TYPE_THEMES
        self.dummy_page._define_page_type(u"/emissions")
        self.assertEqual(self.context.type, self.context.TYPE_THEMES)

        # Use case #4: URL of type TYPE_SHOWS
        self.dummy_page._define_page_type(u"/documentaires/medias")
        self.assertEqual(self.context.type, self.context.TYPE_SHOWS)

        # Use case #5: URL of type TYPE_EPISODES
        self.dummy_page._define_page_type(u"/podcasts/vrac")
        self.assertEqual(self.context.type, self.context.TYPE_EPISODES)

    @patch("resources.lib.webscraper.requests")
    def test_send_request_error(self, requests_mock):
        """Test errors are correctly handled in _send_request

        Only requests.request is mocked because if the full requests
        library is mocked, the "except requests.HTTPError" will
        try to catch a Mock, not a HTTPError."""

        # Since the entire requests module is mocked, mock the exception that
        # would be raised by requests.raise_for_status with an actual exception
        requests_mock.HTTPError = Exception

        # Configure raise_for_status() to return the same exception
        requests_mock.request.return_value.raise_for_status.side_effect = \
            Exception

        # Calling the WebScraper constructor will raise an exception that will
        # be caught by context.logger.fatal and then raised again. To prevent
        # the code from keeping on running (because context.logger.fatal is a
        # Mock so it will not raise the exception), define a side effect to
        # actually raise the exception.
        self.context.logger.fatal.side_effect = Exception

        # Try to create an object with an invalid URL to generate an error
        with self.assertRaises(Exception):
            WebScraper(context=self.context, url="")._send_request(
                method="GET",
                url=""
            )

        # Check that the error was correctly detected (assert_called_once_with
        # is not used because the message of the HTTPError is dynamic)
        self.context.logger.fatal.assert_called_once()

    @patch("resources.lib.webscraper.WebScraper._send_request")
    @patch("resources.lib.webscraper.bs4")
    def test_get_page_content(self, bs4, _send_request):
        """Test the expected API are called in _get_page_content"""

        #  Call the method
        self.dummy_page._get_page_content()

        # Check the expect calls were made
        _send_request.assert_called_once_with(method="GET", url="")
        bs4.BeautifulSoup.assert_called_once_with(
            markup=self.dummy_page.http_response.content,
            features="html.parser",
            from_encoding="utf-8"
        )

        # Check the internal variables were set
        self.assertEqual(self.dummy_page.http_response,
                         _send_request.return_value)
        self.assertEqual(self.dummy_page.html_soup,
                         bs4.BeautifulSoup.return_value)

    def test_create_item_info(self):
        """Test the default values and the returned values in _create_item_info
        """

        # Check the default values when no arguments are provided
        self.assertEqual(self.dummy_page._create_item_info(),
                         {
                             "action_type": "",
                             "author": "",
                             "background": "",
                             "date": "",
                             "description": "",
                             "duration": 0,
                             "is_folder": True,
                             "name": "",
                             "thumbnail": "",
                             "url": "",
                         })

        # Check the returned values when all the arguments are used
        d = {"action_type": u"action",
             "author": u"author_name",
             "background": u"background_url",
             "date": u"date_value",
             "description": u"description_string",
             "duration": 42,
             "is_folder": False,
             "name": u"name_value",
             "thumbnail": u"thumbnail_url",
             "url": u"url_value"}
        self.assertEqual(self.dummy_page._create_item_info(**d), d)

    @patch("resources.lib.webscraper.WebScraper.get_home_page_items")
    @patch("resources.lib.webscraper.WebScraper._build_items_info")
    @patch("resources.lib.webscraper.WebScraper._get_data_per_type")
    def test_get_items(self, get_data_per_type, build_items_info,
                       get_home_page_items):
        """Test get_items()"""

        # Use case #1: the type of the current page is TYPE_HOME_PAGE
        self.context.type = self.context.TYPE_HOME_PAGE
        self.dummy_page.get_items()
        get_home_page_items.assert_called_once()
        get_data_per_type.assert_not_called()
        build_items_info.assert_not_called()

        # Reset the mock that was called
        get_home_page_items.reset_mock()

        # Use case #é: the type of the current page is NOT TYPE_HOME_PAGE
        self.context.type = 42
        self.dummy_page.get_items()
        get_home_page_items.assert_not_called()
        get_data_per_type.assert_called_once()
        build_items_info.assert_called_once_with(get_data_per_type.return_value)

    @patch("resources.lib.webscraper.WebScraper._create_item_info")
    def test_get_home_page_items(self, create_item_info):
        """Test the returned home page items."""

        self.dummy_page.get_home_page_items()
        create_item_info.assert_has_calls(
            [
                call(name=u"Recherche",
                     url=u"/recherche",
                     action_type=u"search"),
                call(name=u"Emissions",
                     url=u"/emissions",
                     action_type=u"list"),
                call(name=u"Documentaires",
                     url=u"/documentaires",
                     action_type=u"list"),
                call(name=u"Podcasts",
                     url=u"/podcasts",
                     action_type=u"list"),
                call(name=u"Courts-métrages",
                     url=u"/courts-metrages",
                     action_type=u"list"),
                call(name=u"Albums musicaux",
                     url=u"/musique",
                     action_type=u"list")
            ]
        )

    def test_get_data_per_type_unknown_type(self):
        """Test the error case in _get_data_per_type"""

        # Use a non valid value for the type of the current page
        self.context.type = 0

        # Calling _get_data_per_type will raise an exception that will be
        # caught by context.logger.fatal and then raised again. To prevent the
        # code from keeping on running (because context.logger.fatal is a Mock
        # so it will not raise the exception), define a side effect to actually
        # raise the exception and stop the execution.
        self.context.logger.fatal.side_effect = WebScraperError

        # Call _get_data_per_type to generate the error and the exception
        with self.assertRaises(WebScraperError):
            self.dummy_page._get_data_per_type()

        # Check that the error was correctly detected
        self.context.logger.fatal.assert_called_once_with(
            message=u"Unexpected page type ({}) in _get_data_per_type!"
            .format(self.context.type),
            exception=WebScraperError
        )

    def test_build_items_info_unknown_type(self):
        """Test the error case in _build_items_info"""

        # Use a non valid value for the type of the current page
        self.context.type = 0

        # Calling _build_items_info will raise an exception that will be caught
        # by context.logger.fatal and then raised again. To prevent the code
        # from keeping on running (because context.logger.fatal is a Mock so it
        # will not raise the exception), define a side effect to actually raise
        # the exception.
        self.context.logger.fatal.side_effect = WebScraperError

        # Call _build_items_info to generate the error and the exception
        with self.assertRaises(WebScraperError):
            self.dummy_page._build_items_info([{}])

        # Check that the error was correctly detected
        self.context.logger.fatal.assert_called_once_with(
            message=u"Unexpected page type ({}) in _build_items_info!"
            .format(self.context.type),
            exception=WebScraperError
        )

    def test_build_items_info_dialog_cancelled(self):
        """Test when the progress dialog is cancelled in _build_items_info"""

        # The progress dialog is used only for windows of type TYPE_SHOWS
        # so first create a WebScraper object of this type
        shows_page = WebScraper(context=self.context,
                                url=u"/emissions/conscience")

        # _build_items_info needs the list of items so first we generate it
        items = shows_page._get_data_per_type()

        # Update the Mock to simulate a cancel action in the progress dialog
        self.context.kodi.is_dialog_cancelled.return_value = True

        # Call _build_items_info on a TYPE_SHOWS page (the progress dialog
        # is displayed only for this type). It should return an empty list
        # because the "break" instruction will exit the loop at the first
        # iteration, before any item is added to the returned list.
        self.assertEqual(shows_page._build_items_info(items), [])

        # Disable the cancel action for the other test cases
        self.context.kodi.is_dialog_cancelled.return_value = False

    def test_search_results(self):
        """Validate the returned information from a search result.

        This test case validates the values in the dict returned by
        get_items() for this type of window.
        It does not validate the returned data because it may change.
        """

        # Create an object pointing to search results (the keyword used is
        # supposed to return a bookshop link)
        search_page = WebScraper(context=self.context,
                                 url=u"/recherche",
                                 search_keywords=u"data")

        # Check the type of the current window
        self.assertEqual(self.context.type, self.context.TYPE_SHOWS)

        # Get the main categories
        main_categories = search_page.get_items()

        for category in main_categories:
            self.assertNotEqual(category["name"].lower(), "")
            self.assertNotEqual(category["url"], "",
                                msg=u"Wrong 'url' for {}"
                                .format(category["name"]).encode("utf-8"))
            self.assertTrue(category["is_folder"],
                            msg=u"Wrong 'is_folder' for {}"
                            .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["action_type"], "list",
                             msg=u"Wrong 'action_type' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertNotEqual(category["thumbnail"], "",
                                msg=u"Wrong 'thumbnail' for {}"
                                .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["background"], "",
                             msg=u"Wrong 'background' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertNotEqual(category["description"], "",
                                msg=u"Wrong 'description' for {}"
                                .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["author"], "",
                             msg=u"Wrong 'author' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["duration"], 0,
                             msg=u"Wrong 'duration' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["date"], "",
                             msg=u"Wrong 'date' for {}"
                             .format(category["name"]).encode("utf-8"))

    def test_main_categories(self):
        """Validate the returned information about the main categories

        This test case validates:
         - the returned data by the HTTP request
         - the values in the dict returned by get_items()
        """

        # Create an object of type TYPE_HOME_PAGE
        main_page = WebScraper(context=self.context,
                               url=u"")

        # Check the type of the current window
        self.assertEqual(self.context.type, self.context.TYPE_HOME_PAGE)

        expected_categories = [u"recherche",
                               u"emissions",
                               u"documentaires",
                               u"courts-métrages",
                               u"podcasts",
                               u"albums musicaux"]

        # Get the main categories
        main_categories = main_page.get_items()

        for category in main_categories:
            self.assertIn(category["name"].lower(), expected_categories)
            self.assertNotEqual(category["url"], "",
                                msg=u"Wrong 'url' for {}"
                                .format(category["name"]).encode("utf-8"))
            self.assertTrue(category["is_folder"],
                            msg=u"Wrong 'is_folder' for {}"
                            .format(category["name"]).encode("utf-8"))
            if category["name"].lower() == u"recherche":
                self.assertEqual(category["action_type"], "search",
                                 msg=u"Wrong 'action_type' for {}"
                                 .format(category["name"]).encode("utf-8"))
            else:
                self.assertEqual(category["action_type"], "list",
                                 msg=u"Wrong 'action_type' for {}"
                                 .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["thumbnail"], "",
                             msg=u"Wrong 'thumbnail' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["background"], "",
                             msg=u"Wrong 'background' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["description"], "",
                             msg=u"Wrong 'description' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["author"], "",
                             msg=u"Wrong 'author' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["duration"], 0,
                             msg=u"Wrong 'duration' for {}"
                             .format(category["name"]).encode("utf-8"))
            self.assertEqual(category["date"], "",
                             msg=u"Wrong 'date' for {}"
                             .format(category["name"]).encode("utf-8"))

    def test_themes(self):
        """Validate the returned information about the themes

        This test case validates the values in the dict returned by
        get_items() for this type of window.
        It does not validate the returned data because it may change.
        """

        # Create an object of type TYPE_THEMES
        themes_page = WebScraper(context=self.context,
                                 url=u"/emissions")

        # Check the type of the current window
        self.assertEqual(self.context.type, self.context.TYPE_THEMES)

        # Get the themes
        themes = themes_page.get_items()

        for theme in themes:
            self.assertNotEqual(theme["name"].lower(), "")
            self.assertNotEqual(theme["url"], "",
                                msg=u"Wrong 'url' for {}"
                                .format(theme["name"]).encode("utf-8"))
            self.assertTrue(theme["is_folder"],
                            msg=u"Wrong 'is_folder' for {}"
                            .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["action_type"], "list",
                             msg=u"Wrong 'action_type' for {}"
                             .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["thumbnail"], "",
                             msg=u"Wrong 'thumbnail' for {}"
                             .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["background"], "",
                             msg=u"Wrong 'background' for {}"
                             .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["description"], "",
                             msg=u"Wrong 'description' for {}"
                             .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["author"], "",
                             msg=u"Wrong 'author' for {}"
                             .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["duration"], 0,
                             msg=u"Wrong 'duration' for {}"
                             .format(theme["name"]).encode("utf-8"))
            self.assertEqual(theme["date"], "",
                             msg=u"Wrong 'date' for {}"
                             .format(theme["name"]).encode("utf-8"))

    def test_shows(self):
        """Validate the returned information about the shows

        This test case validates the values in the dict returned by
        get_items() for this type of window.
        It does not validate the returned data because it may change.
        """

        # Create an object of type TYPE_SHOWS
        shows_page = WebScraper(context=self.context,
                                url=u"/emissions/conscience")

        # Check the type of the current window
        self.assertEqual(self.context.type, self.context.TYPE_SHOWS)

        # Get the list of shows
        shows = shows_page.get_items()

        for show in shows:
            self.assertNotEqual(show["name"].lower(), "")
            self.assertNotEqual(show["url"], "",
                                msg=u"Wrong 'url' for {}"
                                .format(show["name"]).encode("utf-8"))
            self.assertTrue(show["is_folder"],
                            msg=u"Wrong 'is_folder' for {}"
                            .format(show["name"]).encode("utf-8"))
            self.assertEqual(show["action_type"], "list",
                             msg=u"Wrong 'action_type' for {}"
                             .format(show["name"]).encode("utf-8"))
            self.assertNotEqual(show["thumbnail"], "",
                                msg=u"Wrong 'thumbnail' for {}"
                                .format(show["name"]).encode("utf-8"))
            self.assertEqual(show["background"], "",
                             msg=u"Wrong 'background' for {}"
                             .format(show["name"]).encode("utf-8"))
            self.assertNotEqual(show["description"], "",
                                msg=u"Wrong 'description' for {}"
                                .format(show["name"]).encode("utf-8"))
            self.assertEqual(show["author"], "",
                             msg=u"Wrong 'author' for {}"
                             .format(show["name"]).encode("utf-8"))
            self.assertEqual(show["duration"], 0,
                             msg=u"Wrong 'duration' for {}"
                             .format(show["name"]).encode("utf-8"))
            self.assertEqual(show["date"], "",
                             msg=u"Wrong 'date' for {}"
                             .format(show["name"]).encode("utf-8"))

    def test_episodes(self):
        """Validate the returned information about the episodes

        This test case validates the values in the dict returned by
        get_items() for this type of window.
        It does not validate the returned data because it may change.
        However currently it helps to test (almost)all the use cases in the
        method _build_items_info: extracts, teaser, bonus and movie
        """

        # In order to cover all the cases for TYPE_EPISODES, use several URLs
        episodes_url = [
            # A docu that has a almost everything: teaser, movie, extracts
            # and bonus
            u"/documentaires/en-quete-de-sens",
            # An "emission" which episodes duration is formatted with
            # hours
            u"/emissions/c-est-quoi-le-bonheur",
            # A "court-métrage"
            u"/courts-metrages/je-suis-contradictoire"
        ]

        # Loop on all the URLs
        for url in episodes_url:

            # Create the object of type TYPE_EPISODES
            episodes_page = WebScraper(context=self.context, url=url)

            # Check the type of the current window
            self.assertEqual(self.context.type, self.context.TYPE_EPISODES)

            # Rewrite the GlobalContext.unicode function to increase code
            # coverage (otherwise several lines in _extract_duration_date won't
            # be executed)
            self.context.unicode.side_effect = \
                (lambda x: x.decode("utf-8") if isinstance(x, bytes) else x)

            for episode in episodes_page.get_items():
                self.assertNotEqual(episode["name"].lower(), "")
                self.assertNotEqual(episode["url"], "",
                                    msg=u"Wrong 'url' for {}"
                                    .format(episode["name"]).encode("utf-8"))
                self.assertFalse(episode["is_folder"],
                                 msg=u"Wrong 'is_folder' for {}"
                                 .format(episode["name"]).encode("utf-8"))
                self.assertEqual(episode["action_type"], "play",
                                 msg=u"Wrong 'action_type' for {}"
                                 .format(episode["name"]).encode("utf-8"))
                self.assertNotEqual(episode["thumbnail"], "",
                                    msg=u"Wrong 'thumbnail' for {}"
                                    .format(episode["name"]).encode("utf-8"))
                self.assertNotEqual(episode["background"], "",
                                    msg=u"Wrong 'background' for {}"
                                    .format(episode["name"]).encode("utf-8"))
                self.assertNotEqual(episode["description"], "",
                                    msg=u"Wrong 'description' for {}"
                                    .format(episode["name"]).encode("utf-8"))
                self.assertNotEqual(episode["author"], "",
                                    msg=u"Wrong 'author' for {}"
                                    .format(episode["name"]).encode("utf-8"))

                if ("/bande-annonce/" in episode["url"].lower()
                        or "/extraits/" in episode["url"].lower()
                        or "/bonus/" in episode["url"].lower()):
                    self.assertEqual(episode["duration"], 0,
                                     msg=u"Wrong 'duration' for {}"
                                     .format(episode["name"]).encode("utf-8"))
                else:
                    self.assertNotEqual(
                        episode["duration"], 0,
                        msg=u"Wrong 'duration' for {}"
                        .format(episode["name"]).encode("utf-8"))

                self.assertNotEqual(episode["date"], "",
                                    msg=u"Wrong 'date' for {}"
                                    .format(episode["name"]).encode("utf-8"))

    @patch("resources.lib.webscraper.re.compile")
    def test_extract_episod_info(self, re_compile):
        """Test the error use case in _extract_episod_info"""

        # WebScraper's self.http_response.text is required for this test but
        # self.http_response is None in self.dummy_page so we mock this
        # attribute
        back_up_http_response = self.dummy_page.http_response
        self.dummy_page.http_response = Mock()

        # Simulate the regexp didn't match
        re_compile.return_value.search.return_value = False

        # Call the method and check its returned value
        self.assertEqual(self.dummy_page._extract_episod_info(),
                         {"content_id": u"null",
                          "section_id": u"null",
                          "episod_id": u"null"
                         })

        # Check if the error logging function was called
        self.context.logger.error.assert_has_calls(
            [
                call(u"Couldn't find the variable content_id"),
                call(u"Couldn't find the variable section_id"),
                call(u"Couldn't find the variable episod_id")
            ],
            any_order=True)

        # Revert the changes done to self.dummy_page.http_response
        self.dummy_page.http_response = back_up_http_response

    @patch("resources.lib.webscraper.WebScraper._send_request")
    def test_get_episod_info_error(self, send_request):
        """Test the error use case in _get_episod_info"""

        # Mock the returned value by send_request to simulate the error use
        # case
        send_request.return_value.json.return_value = {
            "video_id": u"",
            "audio_id": u""
        }

        # Call the method and check its returned value
        internal_id = {"foo": u"bar"}
        self.assertEqual(self.dummy_page._get_episod_info(internal_id),
                         u"unknown")

        # Check that send_request was called with the arguments
        send_request.assert_called_once_with(method="POST",
                                             url="ws/get_episod_info.php",
                                             data=internal_id)

    def test_get_playable_url_youtube(self):
        """Validate get_playable_url returns a valid YouTube URL."""

        # Create an object on a documentary hosted on YouTube
        youtube_page = WebScraper(
            context=self.context,
            url=u"/documentaires/j-ai-pas-vote/film/1")

        # Expected video ID for youtube_page
        video_id = u"uzcN-0Bq1cw"

        # Get the URL of the video
        youtube_page.get_playable_url()

        # Check that the API to build the URL was correctly called
        self.context.kodi.build_kodi_url.assert_called_once_with(
            plugin_url=u"plugin://plugin.video.youtube/play/",
            parameters={"video_id": video_id}
        )

    def test_get_playable_url_vimeo(self):
        """Validate get_playable_url returns a valid Vimeo URL."""

        # Create an object on a short film hosted on Vimeo
        vimeo_page = WebScraper(
            context=self.context,
            url=u"/courts-metrages/apis-mellifera/film/1")

        # Expected video ID for vimeo_page
        video_id = u"262352022"

        # Get the URL of the video
        vimeo_page.get_playable_url()

        # Check that the API to build the URL was correctly called
        self.context.kodi.build_kodi_url.assert_called_once_with(
            plugin_url=u"plugin://plugin.video.vimeo/play/",
            parameters={"video_id": video_id}
        )

    def test_get_playable_url_dailymotion(self):
        """Validate get_playable_url returns a valid dailymotion URL."""

        # Create an object on a short film hosted on Dailymotion
        dailymotion_page = WebScraper(context=self.context,
                                      url=u"/emissions/asteral/1")

        # Expected video ID for dailymotion_page
        video_id = u"k2d5yZxLHCbF5wvVHml"

        # Get the URL of the video
        dailymotion_page.get_playable_url()

        # Check that the API to build the URL was correctly called
        self.context.kodi.build_kodi_url.assert_called_once_with(
            plugin_url=u"plugin://plugin.video.dailymotion_com/",
            parameters={"mode": u"playVideo", "url": video_id}
        )

    def test_get_playable_url_arte(self):
        """Validate get_playable_url returns a valid Arte URL."""

        # Create an object on a short film hosted on Arte.tv
        arte_page = WebScraper(context=self.context,
                               url=u"/documentaires/tous-surveilles/film/1")

        # Expected video ID for arte_page
        video_id = u"083310-000-A"

        # Get the URL of the video
        self.assertEqual(arte_page.get_playable_url(),
                         u"plugin://plugin.video.arteplussept/play/SHOW/{}"\
                         .format(video_id))

    def test_get_playable_url_peertube(self):
        """Validate get_playable_url with an unsupported video service."""

        # Create an object on a media hosted on PeerTube
        peertube_page = WebScraper(context=self.context,
                                   url=u"/emissions/data-gueule/100")

        # Expected values for peertube_page
        instance = u"framatube.org"
        video_id = u"e81e72cb-9916-4be5-a965-12ce51496c44"

        # Get the URL of the video
        peertube_page.get_playable_url()

        # Check that the API to build the URL was correctly called
        self.context.kodi.build_kodi_url.assert_called_once_with(
            plugin_url=u"plugin://plugin.video.peertube/",
            parameters={
                        "action": u"play_video",
                        "instance": instance,
                        "id": video_id}
        )

    def test_get_playable_url_unsupported_service(self):
        """Validate get_playable_url with an unsupported video service."""

        # Create an object on a media hosted on Facebook
        facebook_page = WebScraper(context=self.context,
                                   url=u"/emissions/ami-des-lobbies/1")

        # In this use case, get_playable_url() will call KodiUtils.get_string()
        # through "self.context.kodi" so we mock the return value of this
        # function with a variable placeholder to avoid a TypeError exception:
        # "coercing to Unicode: need string or buffer, Mock found"
        self.context.kodi.get_string.return_value = "{}"

        # Check that get_playable_url returns "unknown"
        self.assertEqual(facebook_page.get_playable_url(), "unknown")

        # Check that the error was reported
        self.context.logger.error.assert_called_once()
        self.context.kodi.display_popup.assert_called_once()

    def test_get_playable_url_podcast(self):
        """Validate get_playable_url returns a valid podcast URL."""

        # Create an object on a podcast
        podcast_page = WebScraper(context=self.context,
                                  url=u"/podcasts/ca-commence-par-nous/57")

        # Check the returned URL is the expected one
        self.assertEqual(podcast_page.get_playable_url(),
                         u"https://stats.podcloud.fr/ca-commence-par-nous/" \
                         u"s02ep27-open-sciences-participatives/enclosure.mp3")
