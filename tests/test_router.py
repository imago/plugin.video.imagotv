# -*- coding: utf-8 -*-
"""
    Test cases related to the router function of the add-on

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import unittest

try:
    # Python 3.x
    from unittest.mock import Mock, patch
except ImportError:
    from mock import Mock, patch

import xmlrunner # pylint: disable=unused-import
# This import is required for generating the XML test results

from resources.router import router


class TestRouter(unittest.TestCase):
    """Test cases related to the router of the add-on"""

    def setUp(self):
        """Set up actions

        Set "longMessage" to True to print the classic error message and the
        user "msg" from the assert() function in case of failures.

        Create a mock GlobalContext object that will be used in the test cases.
        """
        # In addition to the classic error message when a test case fails,
        # also display the "msg" string from the assert() function
        self.longMessage = True

        # Create a mock of a GlobalContext object to pass to router
        self.context = Mock()

    @patch("resources.router.WebScraper")
    def test_router_no_args(self, webscraper_mock):
        """Test when the router is called when no arguments were provided.

        In this case:
         - the WebScraper constructor must be called with an empty URL
         - kodi.generate_list_of_items() must be called with get_items() of
        the WebScraper instance
        """

        # Mock the plugin_args to an empty dict to simulate that no arguments
        # were passed to the add-on
        self.context.plugin_args = {}

        # Call the router with the mocked context
        router(self.context)

        # Check that the WebScraper constructor was called only once with the
        # expected arguments
        webscraper_mock.assert_called_once_with(
            url="",
            search_keywords=None,
            context=self.context)
        # Check that context.kodi.generate_list_of_items was called only once
        # with the expected arguments
        self.context.kodi.generate_list_of_items.assert_called_once_with(
            webscraper_mock.return_value.get_items())

    @patch("resources.router.WebScraper")
    def test_router_action_list(self, webscraper_mock):
        """Test when the router is called with the action "list".

        In this case:
         - the WebScraper constructor must be called with the url from the
        argument "path"
         - kodi.generate_list_of_items() must be called with get_items() of
        the WebScraper instance
        """

        # Mock the plugin_args to an empty dict to simulate that no arguments
        # were passed to the add-on
        self.context.plugin_args = {
            "action": u"list",
            "path": u"test/url/action/list"}

        # Call the router with the mocked context
        router(self.context)

        # Check that the WebScraper constructor was called only once with the
        # expected arguments
        webscraper_mock.assert_called_once_with(
            url=self.context.plugin_args["path"],
            search_keywords=None,
            context=self.context)
        # Check that context.kodi.generate_list_of_items was called only once
        # with the expected arguments
        self.context.kodi.generate_list_of_items.assert_called_once_with(
            webscraper_mock.return_value.get_items())

    @patch("resources.router.WebScraper")
    def test_router_action_play(self, webscraper_mock):
        """Test when the router is called with the action "play".

        In this case:
         - the WebScraper constructor must be called with the url from the
        argument "path"
         - WebScraper.get_playable_url() must be called
        """

        # Mock the plugin_args to an empty dict to simulate that no arguments
        # were passed to the add-on
        self.context.plugin_args = {
            "action": u"play",
            "path": u"test/url/action/play"}

        # Call the router with the mocked context
        router(self.context)

        # Check that the WebScraper constructor was called only once with the
        # expected arguments
        webscraper_mock.assert_called_once_with(
            url=self.context.plugin_args["path"],
            search_keywords=None,
            context=self.context)
        # Check that WebScraper.get_playable_url() was called once
        webscraper_mock.return_value.get_playable_url.assert_called_once()
        # Check that context.kodi.play_media was called only once with the
        # expected arguments
        self.context.kodi.play_media.assert_called_once_with(
            webscraper_mock.return_value.get_playable_url.return_value)

    @patch("resources.router.WebScraper")
    def test_router_action_search(self, webscraper_mock):
        """Test when the router is called with the action "search".

        In this case:
         - context.kodi.open_input_dialog must be called
         - the WebScraper constructor must be called with the url from the
        argument "path" and search_keywords
         - kodi.generate_list_of_items() must be called with get_items() of
        the WebScraper instance
        """

        # Mock the plugin_args to an empty dict to simulate that no arguments
        # were passed to the add-on
        self.context.plugin_args = {
            "action": u"search",
            "path": u"test/url/action/play"}

        # Call the router with the mocked context
        router(self.context)

        # Check that context.kodi.open_input_dialog was called once
        self.context.kodi.open_input_dialog.assert_called_once_with(
            title=self.context.kodi.get_string()
        )
        # Check that the WebScraper constructor was called only once with the
        # expected arguments
        webscraper_mock.assert_called_once_with(
            url=self.context.plugin_args["path"],
            context=self.context,
            search_keywords=self.context.kodi.open_input_dialog.return_value)
        # Check that context.kodi.generate_list_of_items was called only once
        # with the expected arguments
        self.context.kodi.generate_list_of_items.assert_called_once_with(
            webscraper_mock.return_value.get_items())

    def test_router_unknown_action(self):
        """Test when the router is called with an unknown action

        In this case, context.logger.fatal() must be called.
        """

        # Mock the action with an unknown value to reach the error case
        self.context.plugin_args = {"action": u"unknown"}

        # Call the router with the mocked context and check that logger.fatal()
        # was called only once.
        self.context.logger.fatal.assert_not_called()
        router(self.context)
        self.context.logger.fatal.assert_called_once()
