# -*- coding: utf-8 -*-
"""
    Management of the mocking of Kodi libraries

    All the Kodi python libraries must be mocked for all the tests so they are
    mocked in a single location to simplify the process.
    This file also stores the Mock object associated with each library in a
    variable so that it can be used in the test cases with assert_called_xxx()

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys

try:
    # Python 3.x
    from unittest.mock import Mock
except ImportError:
    from mock import Mock

xbmc_mock = Mock()
sys.modules["xbmc"] = xbmc_mock

sys.modules["xbmcaddon"] = Mock()

xbmcgui_mock = Mock()
sys.modules["xbmcgui"] = xbmcgui_mock

xbmcplugin_mock = Mock()
sys.modules["xbmcplugin"] = xbmcplugin_mock
