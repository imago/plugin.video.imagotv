# General guidelines

Thank you for deciding to contribute to this project :)
Please follow these guidelines when implementing your code.

[[_TOC_]]

## Change workflow

The `mainline` branch contains the latest version of the code. This branch must be stable and working at any time. To ensure this CI pipelines are used.

The workflow is the following:
1. create a branch on the main repository with an explicit name
2. create a merge request from your branch to the `mainline` branch
3. add the `WIP: ` prefix until your merge request is ready
4. when you change is ready, remove the `WIP: ` prefix
5. A pipeline is created each time you push commits in a merge request but it will not start automatically: the user may start it. Since a merge request cannot be merged until the associated pipeline passed, start the `blocked pipeline` associated with the latest commit in your merge request when your change is ready
6. if the pipeline passed, the merge request may be merged. Please try to squash the commits as much as possible.


Note: the pipeline will run tests and quality validation (for more information see the [CI file](../misc/gitlab-ci.yml)).

## Validation

To validate your change in the code you may either:
- use the GitLab pipeline (as explained in [Change workflow](#change-workflow))
- run the tests locally

### Unit Tests

The tests use the following python libraries:
- [unittest](https://docs.python.org/2/library/unittest.html) as test framework
- [coverage](https://coverage.readthedocs.io) as code coverage framework
- [unittest-xml-reporting](https://github.com/xmlrunner/unittest-xml-reporting/) for generating a JUnit report to display test results on GitLab

To run all the tests locally you can use this command:

```python
python -m pip install -r misc/requirements_python2.txt
python -m unittest discover --verbose
```
More details are available in the [CI file](../misc/gitlab-ci.yml).

### Quality Checks

The `quality` job run:
- [Kodi add-on checker](https://github.com/xbmc/addon-check) to validate the add-on follows Kodi's best practices
- [pylint](https://www.pylint.org/) to analyze the code

These tools can be invoked locally with:

```python
python3 -m pip install -r misc/requirements_python3.txt
kodi-addon-checker --branch leia
python3 -m pylint <options>
```

More details are available in the [CI file](../misc/gitlab-ci.yml).

## Code style

- The code follows [PEP 8](https://www.python.org/dev/peps/pep-0008/) conventions. The compliance can be checked with [pylint](https://www.pylint.org/) as explained in [this part](#quality-checks).
- PEP 8 does not recommend double quotes over single quotes for strings definition
but please use only double quotes as single quotes are common characters in some
languages (French and English for instance). Of course when double quotes are
part of the string, it is better to use single quotes around the string to
avoid escape characters.
- Imago TV references only French speaking content but the code must
be documented only in English to allow anybody to contribute. And who knows how the website will evolve :)

## Strings, logging and python2/python3 compatibility

The development of this add-on started when Kodi official release was v18 (Leia).
This version used python2 but to prepare the migration to python3
in the next Kodi release (v19 Matrix) this add-on was (partially) made compatible with both python2 and python3.

To keep this compatibility until Kodi v19 is officially released here are some rules to follow:
- Respect the [unicode sandwich](http://bit.ly/unipain). Long story short: convert all the strings to unicode strings in the internal code of this add-on. Convert all the unicode strings to byte strings when calling external libraries or code. Most of the time it simply means prefixing the string with "u" and decoding byte string with UTF-8
- Do not use print even for debugging. In Kodi print() is a wrapper on xbmc.log() but xbmc.log() expects byte strings only which may lead to [very strange error](https://forum.kodi.tv/showthread.php?tid=353580) in case you use print(). The Logger was made to avoid issues and must be used even for printing debug messages.

## Release of a new version

Here are the steps to release a new version:

1. Create a release branch whose name follows this format: `release/<release_name>`
2. On this branch don't commit any new feature. Only commit changes related to the release process like:
  - a bump of the add-on version in `addon.xml` (note that the version numbering must follow the [semantic versioning](https://semver.org/))
  - the update of the changelog in the `news` tag in `addon.xml` (using Markdown syntax since it will be re-used automatically in the release notes)
  - update the latest version in the logo in README.md
3. Merge the merge request (a validation pipeline must be run)
4. A new pipeline with the job `create-release` will be created: run the job manually since it should be `blocked`.
5. The new release will be available on the Releases page.