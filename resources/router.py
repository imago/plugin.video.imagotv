# -*- coding: utf-8 -*-
"""
    Main router function of the add-on

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from resources.lib.webscraper import WebScraper


def router(context):
    """Route to the correct action based on the arguments received."""

    # Get the value of the requested action from the called URL
    action = context.plugin_args.get("action", "list")
    path = context.plugin_args.get("path", "")
    # Note: if it is the initial call of the add-on, the arguments will be set
    # with default value that will enable the WebScraper class to return the
    # items of the home page.

    # In case the action is "search", ask the user which keywords to search for
    if action == u"search":
        user_keywords = context.kodi.open_input_dialog(
            title=context.kodi.get_string(30204))
    else:
        user_keywords = None

    # Instantiate the WebScraper class with the arguments
    imago = WebScraper(context=context,
                       url=path,
                       search_keywords=user_keywords)

    # In case the debug mode is enabled, display some variables (can't be done
    # before because we want to display the type of the current page which
    # is defined when instantiating WebScraper)
    debug_dict = {"type": context.type}
    debug_dict.update(context.plugin_args)
    context.kodi.display_popup(title=u"Variables",
                               message=str(debug_dict),
                               only_debug=True)

    if action in [u"list", u"search"]:
        # Common branch for the home page and the actions "list" and "search"
        # ==> display in Kodi GUI the items of the associated webpage
        context.kodi.generate_list_of_items(imago.get_items())
    elif action == u"play":
        # "play" action ==> get the URL of the media and play it
        context.kodi.play_media(imago.get_playable_url())
    else:
        context.logger.fatal(message=u"Unknown action '{}'"
                             .format(action),
                             exception=NameError)
