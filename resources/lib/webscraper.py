# -*- coding: utf-8 -*-
"""
    Library used to scrap the content of the ImagoTV website.

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import re

import bs4
import requests
from requests.compat import urljoin, quote


class WebScraperError(Exception):
    """Simple class for exceptions raised by the WebScraper class."""


class WebScraper():
    """Class used to scrap the content of the website

    Basically the plugin may request 4 different types of items:
     - TYPE_HOME_PAGE: home page of the add-on
     - TYPE_THEMES: the themes under each item of type TYPE_HOME_PAGE
     - TYPE_SHOWS: the items of this types are the shows in a subcategory
     - TYPE_EPISODES: the list of episodes for a given show
    """

    # Base URL of the website
    BASE_URL = u"https://www.imagotv.fr"

    # Header used when sendig HTTP requests
    HEADER = {
        "User-Agent": u"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) "
                      u"Gecko/20100101 Firefox/68.0",
        "Accept": u"*/*",
        "Accept-Language": u"fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
        "Accept-Encoding": u"gzip, deflate, br",
        "Referer": urljoin(BASE_URL, u"/")
    }

    # Timeout to limit the number of retries per HTTP request
    TIMEOUT = 5  # seconds

    # List of the possible values for the category in the URL
    CATEGORIES = (u"conscience",
                  u"medias",
                  u"alternatives",
                  u"sante",
                  u"ecologie",
                  u"economie",
                  u"societe",
                  u"histoire")


    def __init__(self, context, url="", search_keywords=None):
        """Constructor of the class

        Arguments:
         - context: the GlobalContext object
         - url: the requested URL relative to BASE_URL with a leading slash
         - search_keywords: the keywords to use for the search

        Initialize the context and logger object.
        Define the type of the requested page that will define the format of
        the data to extract.
        Build the URL of the requested page with search keywords.
        Get the content of the requested page.
        """

        # Store the GlobalContext object
        self.context = context
        # Create a direct access to the logger to ease the future access
        self.logger = self.context.logger

        # Define the type of the current page based on the requested URL
        self._define_page_type(url)

        self.url = url
        # If there are search keywords, join them with the requested URL
        if search_keywords:
            self.url = u"/".join([url.rstrip(u"/"), search_keywords])

        # Get the content of the requested page (nothing is done for the home
        # page because its content is hard-coded)
        if self.context.type != self.context.TYPE_HOME_PAGE:
            self._get_page_content()
        else:
            self.http_response = None
            self.html_soup = None

    def _define_page_type(self, page_url):
        """Define the type of the current page

        The type of the current page will be used in several places of the
        add-on. In this class it will be used to define the format of the data
        that will be searched for.
        """
        if page_url:
            if u"recherche" in page_url:
                # In case the requests page is a search result page, the format
                # of the page is the same as TYPE_SHOWS
                self.context.type = self.context.TYPE_SHOWS
            elif page_url.count("/") == 1:
                # When only the category is in the URL, type is TYPE_THEMES
                self.context.type = self.context.TYPE_THEMES
            else:
                # Otherwise the type may be TYPE_SHOWS or TYPE_EPISODES.

                # If the last part of the URL is one of the existing categories
                # then the type is TYPE_SHOWS
                if page_url.split("/")[2] in self.CATEGORIES:
                    self.context.type = self.context.TYPE_SHOWS
                # Otherwise the type is TYPE_EPISODES
                else:
                    self.context.type = self.context.TYPE_EPISODES
        else:
            # When there is no requested URL, the type is TYPE_HOME_PAGE
            self.context.type = self.context.TYPE_HOME_PAGE

    def _send_request(self, method, url, data=None):
        """Send a HTTP request on the requested URL

        method: GET, POST, PUT, etc.
        url: unicode string of the request URL (relative to BASE_URL)
        data: data to send (optional)

        HTTPError exception will be raised in case of error during the request.
        """

        # Escape the special characters and spaces with quote()
        url = quote(url.encode("utf-8"))
        # Note: we want url to be a unicode string but in python2 "quote()"
        # does not handle properly unicode strings containing non-ASCII
        # characters so it is required to encode url to a byte string
        # before calling "quote()".

        # Build the URL of the requested page by joining the BASE_URL with url
        url = urljoin(self.BASE_URL, url)
        # Note: "urljoin()" will return a unicode string since at least one of
        # the two arguments (BASE_URL) is a unicode string.

        # Extract the content of the requested page
        response = requests.request(method=method,
                                    url=url.encode("utf-8"),
                                    headers=self.HEADER,
                                    timeout=self.TIMEOUT,
                                    data=data)
        # Note about encoding:
        #  - "response.text" is a unicode string in both python 2 and
        # python 3 so can use it directly in our code (cf unicode sandwich).
        #  - "response.content" is a byte string in both python 2 and python 3
        # It may be used to decode a string to be sure which encoding was used.
        #  - "response.encoding" returns the encoding used to create
        # "response.text"

        # Use Request.raise_for_status() to raise an exception if the HTTP
        # request didn't succeed
        try:
            response.raise_for_status()
        except requests.HTTPError as exception:
            self.logger.fatal(
                message=u"Error when sending a {} request on {} with {}"
                .format(method, url, data),
                exception=exception,
                use_exception_message=True)

        return response

    def _get_page_content(self):
        """Get the content of current page

        Get the code of the HTML page and create a Beautiful Soup object to
        be able to parse the content easily.
        """

        # Send a GET request on the URL of the current page
        self.http_response = self._send_request(method="GET", url=self.url)

        # Parse the HTML code with BeautifulSoup4 with this configuration:
        #  - use python's built-in HTML parser to not add any dependency
        #  - the encoding is forced set to "UTF-8" so that we know which
        # encoding was used (it should be enough since the website is in
        # French). Because of this we give bs4 byte strings (with .content) so
        # that the byte strings can be correclty decoded with UTF-8.
        self.html_soup = bs4.BeautifulSoup(
                        markup=self.http_response.content,
                        features="html.parser",
                        from_encoding="utf-8")
        # Note: html_soup.find(...).get or html_soup.find(...).text is a
        # unicode string in both python 2 and python 3 so we can use it
        # directly in our code (cf unicode sandwich).

    def _create_item_info(self,
                          action_type="",
                          author="",
                          background="",
                          date="",
                          description="",
                          duration=0,
                          is_folder=True,
                          name="",
                          thumbnail="",
                          url=""):
        """Create a dict with the information of an item.

        The keys are the one expected by the KodiUtils class:
        - action_type: the type of action for the next call of the plugin
        - author: the author of the item
        - background: the URL of the image to use as background
        - date: the date of the item
        - description: the description of the item
        - duration: the duration of the item
        - is_folder: True if the item is a folder, False if it is a video
        - name: the name of the item
        - thumbnail: the URL of the image to use as thumbnail
        - url: the URL the item points to
        """
        return(
            {
                "action_type": action_type,
                "author": author,
                "background": background,
                "date": date,
                "description": description,
                "duration": duration,
                "is_folder": is_folder,
                "name": name,
                "thumbnail": thumbnail,
                "url": url,
            }
        )

    def get_items(self):
        """Get the items of the requested page based on type of the page

        The format of the dictionnary that is returned is the one from
        _create_item_info
        """

        if self.context.type == self.context.TYPE_HOME_PAGE:
            items = self.get_home_page_items()
        else:
            items = self._build_items_info(self._get_data_per_type())

        return items

    def get_home_page_items(self):
        """Return the list of items for the home page of the add-on"""

        return [
            self._create_item_info(name=u"Recherche",
                                   url=u"/recherche",
                                   action_type=u"search"),
            self._create_item_info(name=u"Emissions",
                                   url=u"/emissions",
                                   action_type=u"list"),
            self._create_item_info(name=u"Documentaires",
                                   url=u"/documentaires",
                                   action_type=u"list"),
            self._create_item_info(name=u"Podcasts",
                                   url=u"/podcasts",
                                   action_type=u"list"),
            self._create_item_info(name=u"Courts-métrages",
                                   url=u"/courts-metrages",
                                   action_type=u"list"),
            self._create_item_info(name=u"Albums musicaux",
                                   url=u"/musique",
                                   action_type=u"list"),
        ]

    def _get_data_per_type(self):
        """Extract the requested data depending on the type of the page

        The format of the data to extract depends on the type of items:
         - TYPE_THEMES: for this type the search of the items is based on the
        "a" tags which have the class "title".
         - TYPE_SHOWS: the search is based on the "div" tags with the class
        "thumbnail". The name of the item has to be retrieved from the
        associated TYPE_EPISODES page using the link in the "a" tag
         - TYPE_EPISODES: the search is based on "div" tags of the class
        "series_thumbnail series_XXXX" where XXXX may be "panorama", "squared"
        or "portrait".
        """

        # Init the list of returned items
        items = []

        if self.context.type == self.context.TYPE_THEMES:
            items = self.html_soup.find_all(
                name="a",
                attrs={"class": "title"})
        elif self.context.type == self.context.TYPE_SHOWS:
            items = self.html_soup.find_all(
                name="div",
                attrs={"class": "thumbnail"})
        elif self.context.type == self.context.TYPE_EPISODES:
            items = self.html_soup.find_all(
                name="div",
                attrs={"class": re.compile(r"series_thumbnail series_.*")})
        else:
            # Unsupported type of the page (we should never end here!)
            self.logger.fatal(
                message=u"Unexpected page type ({}) in _get_data_per_type!"
                .format(self.context.type),
                exception=WebScraperError)

        return items

    def _build_items_info(self, items):
        """Build the information of each items that will be used to create
           folders or files

        The dictionnary that is built for each item has the following keys:
         - name: the name of the item
         - url: the URL the item points to
         - is_folder: True if the item is a folder, False if it is a video
         - action_type: the type of action for the next call of the plugin
         - thumbnail: the URL of the image to use as thumbnail
         - background: the URL of the image to use as background
         - description: the description of the item
         - author: the author of the tiem
         - duration: the duration of the item
         - date: the date of the item
        """

        # Init of the returned list of dictionnaries
        items_info = []

        # Display a windows with a progress bar for the type TYPE_SHOWS since
        # it takes quite long to retrieve all the items of this type.
        if self.context.type == self.context.TYPE_SHOWS:
            self.context.kodi.create_dialog_progress(
                title=self.context.kodi.get_string(30202),
                message=self.context.kodi.get_string(30203)
            )

        # Define the action that will be called when each item is opened. This
        # action is the same for all the items of a given type so it can be
        # defined out of the loop.
        if self.context.type == self.context.TYPE_EPISODES:
            action_type = u"play"
        else:
            action_type = u"list"
        # Then define the rest of the information for each item
        for index, item in enumerate(items):

            # Initialize the info dictionnary for the current item with the
            # information that are already known. The other fields will be set
            # below.
            d = self._create_item_info(
                action_type=action_type,
                is_folder=(self.context.type != self.context.TYPE_EPISODES))

            if self.context.type == self.context.TYPE_THEMES:
                # For this type, the name and the URL of the item are directly
                # available in the page. We just need to clean up the name
                d["name"] = self._clean_category_name(item.text)
                d["url"] = item.get("href")

            elif self.context.type == self.context.TYPE_SHOWS:
                # For this type the URL is directly available on the page
                d["url"] = item.a.get("href")
                # The type TYPE_SHOWS is used for the search results. Some
                # keywords return links outside of Imago (for instance on
                # bookshops) so the associated items are skipped because they
                # are useless in the add-on.
                if d["url"].startswith(u"http"):
                    continue
                # The thumbnail is inserted with an "img" tag which class is
                # either "thumbnail portrait" or "thumbnail panorama". The URL
                # is the "lazy-src" attribute.
                d["thumbnail"] = item.find(
                    name="img",
                    attrs={"class": re.compile(r"thumbnail .*")})\
                    .get("lazy-src").strip()
                # But the name and the description of the items need to be
                # retrieved from the page pointed by the URL...
                # (which decreases the performance)
                show_page = bs4.BeautifulSoup(
                    markup=self._send_request(method="GET",
                                              url=d["url"]).content,
                    features="html.parser",
                    from_encoding="utf-8")
                # Then use the metadata to get the title and the description
                d["name"] = show_page.find(
                    name="meta",
                    attrs={"property": "og:title"}).get("content").strip()
                d["description"] = show_page.find(
                    name="meta",
                    attrs={"property": "og:description"})\
                    .get("content").strip()

                # Check if the user asked to cancel this action
                if self.context.kodi.is_dialog_cancelled():
                    # If the user did, exit the loop with the current info
                    break
                else:
                    # Otherwise update the progress bar
                    self.context.kodi.update_dialog_progress(
                        percent=int(index*100/len(items)))

            elif self.context.type == self.context.TYPE_EPISODES:
                # For this type first get the URL as it will help to
                # differentiate between the teasers and the actual movie in
                # some cases. The URL is in the value of the "href" attribute
                # in the "a" tag which id is "link".
                tag_for_url = item.find(name="a", attrs={"id": "link"})
                if tag_for_url is None:
                    # If no tag could be found, skip the item: it is probably a
                    # link that cannot be played (for instance a "/shop/" link)
                    continue
                else:
                    # If a tag was found, the URL is in the "href" attribute
                    d["url"] = tag_for_url.get("href")

                # Check if this link points to a teaser, an extract, a bonus,
                #  a movie or a show. If it does we are not on a show page.
                show_page = False
                if u"/bande-annonce/" in d["url"].lower():
                    # If it is a teaser, manually name the item
                    d["name"] = u"La bande-annonce"
                elif u"/film/" in d["url"].lower():
                    # If it is the actual movie, manually name the item
                    d["name"] = u"Le film"
                elif u"/extraits/" in d["url"].lower():
                    # Ignore the number of the extract and name it manually
                    d["name"] = u"Extrait"
                elif u"/bonus/" in d["url"].lower():
                    # To simplify the code, this item is named manually.
                    d["name"] = u"Bonus"
                else:
                    # This is probably an episode so we are on a show page.
                    show_page = True
                    # The name of the episode is
                    # is in a sub "a" tag which class is "series_line_1"
                    d["name"] = item.find(
                        name="a",
                        attrs={"class": "series_line_1"}).text.strip()
                # There is no description per episode/video so we use the
                # general description of the show/movie for all the items
                # on this page
                d["description"] = self.html_soup.find(
                    name="meta",
                    attrs={"property": "og:description"})\
                    .get("content").strip()
                # The list of author for the show/movie is in the "a" tag with
                # the class "author". This value will be the same for all the
                # episodes/videos
                d["author"] = self.html_soup.find(
                    name="a",
                    attrs={"class": "author"}).text.strip()
                # The thumbnail URL is in an "img" tag in the attribute
                # "lazy-src"
                d["thumbnail"] = item.find(
                    name="img",
                    attrs={
                        "class": re.compile(r"series_thumbnail series_.*")
                        })\
                    .get("lazy-src").strip()
                # The background image is also in an "img" tag but we need to
                # search it in the whole page, not in "item"
                img_tag = self.html_soup.find(
                    name="img",
                    attrs={"class": "background_image"})
                # In some pages, the background image belongs to the class
                # "movie_background_image"
                if img_tag is None:
                    img_tag = self.html_soup.find(
                        name="img",
                        attrs={"class": "movie_background_image"})
                # Get the URL of the background image which is in the "src"
                # attribute
                d["background"] = img_tag.get("src").strip()

                # The duration and the date are stored differently whether the
                # media is a show or a movie
                if show_page:
                    # In the case of a show, the duration and the date are in
                    # the "a" tag with the class "series_line_2". Get the value
                    # and use a regexp to extract the information.
                    extracted_info =\
                        self._extract_duration_date(item.find(
                            name="a",
                            attrs={"class": "series_line_2"}).text.strip())
                    d["duration"] = extracted_info["duration"]
                    d["date"] = extracted_info["date"]
                else:
                    # For movies and docu, the duration and the date are in 2
                    # different tags.

                    # Set the duration only for the movie
                    if d["name"] == u"Le film":
                        duration = self.html_soup.find(
                            name="a",
                            attrs={"class": "duration"}).text.strip()

                        duration_match =\
                            re.compile(r"(\d+) .*").match(duration)
                        if duration_match:
                            d["duration"] = duration_match.group(1)

                    year = self.html_soup.find(
                        name="a",
                        attrs={"class": "year"}).text.strip()
                    d["date"] = "{}-01-01".format(year)

            else:
                # Unsupported type of the page (we should never end here!)
                self.logger.fatal(
                    message=u"Unexpected page type ({}) in"
                            u" _build_items_info!".format(self.context.type),
                    exception=WebScraperError)

            # Now that all the values are defined, add the dictionnary to the
            # list that will be returned.
            items_info.append(d)

        # Close the dialog box with the progress bar
        if self.context.type == self.context.TYPE_SHOWS:
            self.context.kodi.close_dialog_progress()

        return items_info

    def get_playable_url(self):
        """ Create the URL of the media behind an Imago link

        Call get_episod_info to get the ID of the media in order to build the
        URL in Kodi.
        """
        return self._get_episod_info(self._extract_episod_info())

    def _extract_episod_info(self):
        """Extract the information required by get_episod_info

        On the page of an episode extract the database IDs because they are
        required to call get_episod_info."""

        # The IDs of the item in the Imago database are stored as JS variables
        # and consists of 4 variables.
        internal_id = {
            "content_id": u"null",
            "section_id": u"null",
            "episod_id": u"null"
            }

        # Retrieve these JS variables
        # Create a generic regular expression to find any Javascript variable
        js_variable_regex = r'var\s+{var_name}\s+=\s+"(.*)"'
        # Use this regex to find the variables in the HTML code
        for variable in internal_id:
            match = re.compile(js_variable_regex.format(var_name=variable))\
                    .search(self.http_response.text)
            if match:
                internal_id[variable] = match.group(1)
            else:
                self.logger.error(u"Couldn't find the variable {}"
                                  .format(variable))

        self.logger.debug(u"internal_id for the PHP request = "
                          "{}".format(internal_id))

        return internal_id

    def _get_episod_info(self, internal_id):
        """Build the URL of the episode

        Call get_episod_info() and build the URL that will be used in Kodi
        depending on the type of media and the hosting service."""

        # Initialize some variables to manage the error cases
        media_id = u"unknown"
        media_host = u"unknown"
        media_url = u"unknown"

        # Send the POST request with the IDs on get_episod_info
        php_response = self._send_request(
            method="POST",
            url="ws/get_episod_info.php",
            data=internal_id
        )

        # Log the response of the request
        self.logger.debug(message=u"Response of the PHP request = {}\n"
                          .format(php_response.text.strip()))
        # Note: the reponse is not well encoded (it uses "\u00" for unicode
        # characters) but we can't do anything simple to fix it.

        json_response = php_response.json()

        # Check if the media is a video
        if json_response["video_id"]:
            # The media is a video: store its ID
            media_id = json_response["video_id"]

            # Get the video service used to host the video
            media_host = json_response["video_hosting"]

            # Generate the complete URL based on the media host and ID
            if media_host == u"youtube":
                media_url = self.context.kodi.build_kodi_url(
                    plugin_url="plugin://plugin.video.youtube/play/",
                    parameters={"video_id": media_id}
                )
            elif media_host == u"vimeo":
                media_url = self.context.kodi.build_kodi_url(
                    plugin_url="plugin://plugin.video.vimeo/play/",
                    parameters={"video_id": media_id}
                )
            elif media_host == u"dailymotion":
                media_url = self.context.kodi.build_kodi_url(
                    plugin_url="plugin://plugin.video.dailymotion_com/",
                    parameters={"mode": u"playVideo", "url": media_id}
                )
            elif media_host == u"arte":
                media_url = u"plugin://plugin.video.arteplussept/play/SHOW/{}"\
                            .format(media_id)
                # "SHOW" is the value of the "kind" parameter in the API of the
                # Arte add-on. Maybe there are other possible values.
            elif media_host == u"peertube":
                # For videos hosted on PeerTube, the name of the instance is
                # also required (it is stored in the element "sub_hosting")
                media_url = self.context.kodi.build_kodi_url(
                    plugin_url=u"plugin://plugin.video.peertube/",
                    parameters={
                        "action": u"play_video",
                        "instance": json_response["sub_hosting"],
                        "id": media_id})
            else:
                # This video service is not supported currently...
                self.logger.error(u"Tried to play: {}. \n".format(self.url) +
                                  self.context.kodi.get_string(30201)
                                  .format(media_host))
                self.context.kodi.display_popup(
                    title=self.context.kodi.get_string(30200),
                    message=self.context.kodi.get_string(30201)
                    .format(media_host))

        # Check if it is an audio media
        elif json_response["audio_id"]:
            # It is an audio media.
            # The media_host and media_id are useless for audio media because
            # the URL will be directly played by Kodi embedded player.
            # The URL is stored as audio_id in the JSON response
            media_url = json_response["audio_id"]

        else:
            # Unknown media type
            self.logger.error(
                u"Cannot find video_id nor audio_id for {}".format(self.url))

        # Return the playable URL of the media
        return media_url

    def _clean_category_name(self, name):
        """Clean the name of the category before using it in Kodi GUI

        It will:
        - remove any leading or trailing whitespace
        - replace "É" with "E" (otherwise Kodi would sort items whose name
        starts with an "É" after the letter "Z"...) TODO: remove this on
        Kodi v19 Matrix as it is supposed to be solved.
        - remove any brackets that may come from the website
        """

        return re.sub(r"(.+)\s+\([a-zA-Z0-9]+\)", r"\1",
                      name.strip().replace(u"É", u"E"))

    def _extract_duration_date(self, string):
        """Extract the duration and the date based on a regex.

        "string" is supposed to be the value of the tag containing the
        duration and the date. The format is the following:
        <H>h <M> min - <DD> <mmmm> <YYYY>
        where:
         - H is the number of hours in the duration
         - M is the number of minutes in the duration
         - DD is the day number in the date
         - mmmm is the name of the month (in French) in the date
         - YYYY is the year in the date
        """

        # Handle the date and duration in 2 different steps to support the use
        # case when there is only 1 of the 2 information (either the duration
        # or the date)

        # Try to extract the duration
        duration_regex = re.compile(r"(?:(\d+)h)?\s*(\d+)\s*min.*")
        duration_match = duration_regex.match(string.strip())
        # Initialize the duration to 0 (for when the regex does not match and
        # when the values are not correct)
        duration = 0
        if duration_match:
            # Kodi expects the duration in minutes so extract all the values
            # and convert them to minutes
            if duration_match.group(1):
                duration += 60 * int(duration_match.group(1))
            if duration_match.group(2):
                duration += int(duration_match.group(2))

        # Now let's try to extract the date with this regex:
        #  - there is 1 capturing group for each part (day, month and year)
        #  - the month may be written using the full name (e.g. "décembre") or
        # in a shorter version (e.g. "dec."). Because of this we add the "."
        # (dot) in the capturing group #2: it will allow us to know if we have
        # the full or the short name.
        #  - It seems only lower case letters are used but also include the
        # range A-Z in case the month is capitalized.
        #  - the non-ASCII characters that may appear in the French month names
        # are also added ("é" and "û"). Because of this the regex string must
        # be turned into a unicode string to support these non-ASCII characters
        # (the prefix "ur" can't be used because python 3 doesn't support it)
        date_regex = re.compile(
            self.context.unicode(r".*- +(\d{2}) ([A-Za-zûé.]+) +(\d{4})"))

        date_match = date_regex.match(string.strip())
        if date_match:
            day = date_match.group(1)
            month = date_match.group(2)
            year = date_match.group(3)

            # Kodi expects the date in the format YYYY-MM-DD so we need to map
            # the French months name with months number. Let's use a dict:
            month_map = {
                u"janvier": 1, u"février": 2, u"mars": 3, u"avril": 4,
                u"mai": 5, u"juin": 6, u"juillet": 7, u"août": 8,
                u"septembre": 9, u"octobre": 10, u"novembre": 11,
                u"décembre": 12}

            # Check if the short or the full name was used for the month name
            # in order to convert it to a number.
            if "." in month:
                # In case the short name is used for the month, remove the "."
                month = month.replace(".", "")
                # Next parse the map using only the number of characters in
                # the short name of the month that was captured by the regex
                for full_month in month_map:
                    if full_month[:len(month)] == month:
                        month = month_map[full_month]
                        break
                else:
                    # In case the short name of the month couldn't be mapped
                    # to a number, report a warning and assign an error
                    # string to the variable: it will be used later to detect
                    # the error.
                    self.logger.warning(u"Couldn't find the number behind the"
                                        u"month '{month}' in the page '{url}'"
                                        .format(month=month, url=self.url))
                    month = u"not-found"
            else:
                # If the full name was used, simply use the map
                month = month_map[month]

        # Before formatting the date, check if an error occured:
        if not date_match or month == u"not-found":
            # Return an empty string for the date in case the date could not be
            # found
            date = ""
        else:
            # Otherwise return the expected format
            date = u"{year}-{month:02}-{day:02}".format(year=year,
                                                        month=month,
                                                        day=int(day))

        return {"duration": duration, "date": date}
