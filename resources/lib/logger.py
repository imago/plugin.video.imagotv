# -*- coding: utf-8 -*-
"""
    Wrapper on Kodi logging mechanism.

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys

import xbmc  # noqa / Kodistubs is not compatible with python3 / pylint: disable=syntax-error


class Logger():
    """Logger object to simplify logging mechanism with Kodi"""

    def __init__(self, context):
        """Store the name of the add-on for prefixing the log messages."""

        self.context = context
        self.__addon_name = self.context.addon.getAddonInfo('name')
        # We assume getAddonInfo() returns a string which types is the type
        # expected by xbmc.log() so we don't convert it to unicode as
        # __addon_name should not be used outside of this module.

    def fatal(self, message, exception, use_exception_message=False):
        """Create a LOGFATAL message in Kodi's log and raise an exception.

        When raising the exception, reuse the message of the exception if
        use_exception_message is True. Otherwise use "message" (the same
        message as in Kodi's log).
        """
        message = self._prefix(message)
        xbmc.log(message, xbmc.LOGFATAL)

        # Close any dialog box before raising the exception otherwise any
        # opened box would be stuck on the screen
        self.context.kodi.close_dialog_progress()

        if use_exception_message:
            raise exception
        else:
            raise exception(message)

    def error(self, message):
        """Create a LOGERROR message in Kodi's log."""
        xbmc.log(self._prefix(message), xbmc.LOGERROR)

    def debug(self, message):
        """Create a LOGDEBUG message in Kodi's log."""
        xbmc.log(self._prefix(message), xbmc.LOGDEBUG)

    def warning(self, message):
        """Create a LOGWARNING message in Kodi's log."""
        xbmc.log(self._prefix(message), xbmc.LOGWARNING)

    def _convert(self, message):
        """Convert "message" to the type expected by xbmc.log()

        xbmc.log() takes:
         - a byte string in python2 (Kodi v18.x Leia)
         - an unicode string in python3 (Kodi v19.x Matrix and above)
        """

        # Let's check the type of "message" against the type "bytes" to confirm
        # if it is a unicode or a byte string ("bytes" is known in both
        # python 2 and 3)
        if sys.version_info.major == 2:
            if isinstance(message, bytes):
                # If "message" is already a byte string, return it as it is
                return message
            else:
                # Convert "message" to a byte string if it is a unicode string
                return message.encode("utf-8")
        else:
            # For python 3 return "message" converted to a unicode string
            return self.context.unicode(message)

    def _prefix(self, message):
        """Prefix a message with the name of the add-on.

        "message" is converted to avoid encoding/decoding errors.
        """

        return "[{prefix}] {string}".format(prefix=self.__addon_name,
                                            string=self._convert(message))
