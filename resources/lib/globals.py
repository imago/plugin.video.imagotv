# -*- coding: utf-8 -*-
"""
    Global class for exchanging information between objects.

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

try:
    # Python 3.x
    from urllib.parse import parse_qsl
except ImportError:
    # Python 2.x
    from urlparse import parse_qsl

import xbmcaddon

from resources.lib.kodiutils import KodiUtils
from resources.lib.logger import Logger


class GlobalContext():
    """Global class containing general information that needs to exchanged
    between objects.
    """

    # Possible types of the current page
    TYPE_UNDEFINED = 0
    TYPE_HOME_PAGE = 1
    TYPE_THEMES = 2
    TYPE_SHOWS = 3
    TYPE_EPISODES = 4

    def __init__(self, argv):
        """Constructor"""

        # First create a logger object for to be able to log anything from now.
        # The constructor needs an Addon object so we create it too.
        self.addon = xbmcaddon.Addon()
        self.logger = Logger(self)

        # Store the arguments that were passed to the entry point of the add-on
        self.args = argv
        # Split the arguments to ease the reusability:
        # - URL of the add-on (plugin://plugin...) converted to unicode string
        self.plugin_url = self.unicode(self.args[0])
        # - Number of the handle in this call
        self.plugin_handle = int(self.args[1])
        # - URL encoded arguments prefixed with a "?" (which is removed)
        self.plugin_args = dict(parse_qsl(self.args[2][1:]))

        # Get the state of the debug mode from the settings
        self.debug_enabled = self.addon.getSettingBool("debug_mode")

        # Create an object to control easily the Kodi GUI. It must be done last
        # to ensure this object was fully created since it is passed to
        # KodiUtils
        self.kodi = KodiUtils(self)

        # Initialize the type of the page that will be displayed
        self.type = self.TYPE_UNDEFINED

    def unicode(self, string):
        """Convert a string to unicode whatever the python version."""

        # Let's check the type of "string" against the type "bytes" to confirm
        # if it is a unicode or a byte string ("bytes" is known in both
        # python 2 and 3)
        if isinstance(string, bytes):
            # If "string" is a byte string, convert it to unicode
            return string.decode("utf-8")
        else:
            # If "string" is already a unicode string, return it as it is
            return string
