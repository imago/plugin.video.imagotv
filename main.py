# -*- coding: utf-8 -*-
"""
    Entry point of the add-on

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys

from resources.lib.globals import GlobalContext
from resources.router import router

def main_func(args):
    """Function called as entry point of the add-on

    This function was created to be able to test the code in this file.
    """
    if __name__ == "__main__":
        # Create a context object to exchange information between all the
        # modules of the add-on
        context = GlobalContext(argv=args)

        # Call the router function
        router(context)

main_func(sys.argv)
