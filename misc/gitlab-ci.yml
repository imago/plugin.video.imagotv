default:
  # Cancel any pipeline when a newer instance is started (does not seem to work
  # currently for the detached pipelines created from merge requests...)
  interruptible: true

stages:
  - validation
  - release

# Create YAML anchors to avoid code duplicate (note: an anchor cannot call
# another anchor as it will result in an array of array)
.apt_get_update: &apt_get_update
- apt-get update > /dev/null

.python3_prep: &python3_prep
- apt-get install --yes python3-dev python3-pip > /dev/null
- python3 -m pip --quiet install -r misc/requirements_python3.txt

.release_script_prep: &release_script_prep
- apt-get install --yes python3-dev python3-pip git > /dev/null
- git clone https://framagit.org/thombet/scripts-for-kodi-add-ons.git
- python3 -m pip --quiet install -r scripts-for-kodi-add-ons/create-new-add-on-release/requirements.txt

tests:
  stage: validation
  # Do not get any artifacts from previous jobs
  dependencies: []
  rules:
    # Run this job only on merge requests and only as "manual" (it would have
    # been better to configure this at pipeline-level with "workflow" but it
    # does not support "when: manual"...).
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: manual
  before_script:
  - *apt_get_update
  - apt-get install --yes python-dev python-pip > /dev/null
  - python -m pip --quiet install -r misc/requirements_python2.txt
  - *python3_prep
  script:
    - python -m coverage run --source . -m xmlrunner --output-file junit-report.xml discover --verbose
    - python -m coverage report
    - python -m coverage html
    - python3 -m unittest discover --verbose
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    name: "htmlcov-$CI_JOB_ID"
    expose_as: "Code Coverage Report"
    paths:
      - htmlcov/
    expire_in: 1 week
    when: always
    reports:
      junit: junit-report.xml

quality:
  stage: validation
  # Do not get any artifacts from previous jobs
  dependencies: []
  rules:
    # Run this job only on merge requests and only as "manual" (it would have
    # been better to configure this at pipeline-level with "workflow" but it
    # does not support "when: manual"...).
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: manual
  before_script:
  - *apt_get_update
  - *python3_prep
  script:
    - kodi-addon-checker --branch leia --reporter=log --reporter=console
    - find . -iname '*.py' | xargs -t python3 -m pylint --rcfile=misc/pylint-rcfile.txt | tee pylint.log
  artifacts:
    name: "quality-logs-$CI_JOB_ID"
    expose_as: "Quality Logs"
    paths:
      - kodi-addon-checker-report.log
      - pylint.log
    expire_in: 1 week
    when: always

pre-release:
  stage: validation
  # Do not get any artifacts from previous jobs
  dependencies: []
  rules:
    # Run this job only on merge requests for release branches
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^release\//'
      when: manual
  before_script:
  - *apt_get_update
  - *release_script_prep
  script:
    - python3 scripts-for-kodi-add-ons/create-new-add-on-release/create-new-add-on-release.py --dry-run

create-release:
  stage: release
  # Do not get any artifacts from previous jobs
  dependencies: []
  # Run this job only for new commits on the mainline branch and only as "manual"
  # because it is not mandatory to release all the commits on mainline.
  rules:
    - if: '$CI_COMMIT_BRANCH == "mainline"'
      when: manual
    - when: never
  before_script:
  - *apt_get_update
  - *release_script_prep
  script:
    - python3 scripts-for-kodi-add-ons/create-new-add-on-release/create-new-add-on-release.py