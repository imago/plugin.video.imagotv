# ImagoTV Kodi Add-on

[![latest release 1.4.0](https://img.shields.io/badge/latest%20release-1.4.0-blue?style=for-the-badge&logo=skyliner&logoColor=white)](https://framagit.org/imago/plugin.video.imagotv/-/releases/1.4.0)  [![Kodi v18 Leia](https://img.shields.io/badge/Kodi-v18%20Leia-blueviolet?style=for-the-badge)](https://kodi.tv/download)

_______________________________________________________________________________

This Kodi add-on browses and plays the content available on [ImagoTV](https://www.imagotv.fr).

Note: it is an independent project and it was not not developed by ImagoTV team.

[[_TOC_]]

## Features

- List all the audio and video content ("émissions", "documentaires", "courts-métrages", "podcasts" and "albums musicaux").
- Play any audio content
- Play video content hosted on:
  - Arte.tv
  - Dailymotion
  - PeerTube
  - Vimeo
  - YouTube
- Search in the website content (the name of the episodes are currently not included in the search results because it generates too many results)

## Installation

ImagoTV hosts very little content: instead it relies on several different
hosting services.  
This add-on follows the same strategy: the audio content are playable out of
the box by Kodi but this add-on depends on other add-ons to play the videos.  
These add-ons are not all available in the official Kodi add-ons repository so
the easiest way to install the ImagoTV add-on is to install the [Imago
Repository](https://framagit.org/imago/repository.imago) because it contains
all the necessary add-ons.
It will also allow you to get automatic updates.

Another alternative is to use the link `source code (zip)` in one of the
versions available in the
[releases](https://framagit.org/imago/plugin.video.imagotv/-/releases) and to
use the [Install from a ZIP
file](https://kodi.wiki/view/Add-on_manager#How_to_install_from_a_ZIP_file)
feature in Kodi with this file. Note that you will not get automatic updates
with this method and that you will have to install the dependent add-ons
manually.

For a complete list of dependencies, please have a look in [addon.xml](./addon.xml).

## License

This add-on is licensed under the GNU GPLv3 license.
See [LICENSE.txt](./LICENSE.txt) for more information.

## Contributing

Feel free to contribute to this project.
Some guidelines are available for you [here](./docs/contributing.md).